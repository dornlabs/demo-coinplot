package coinplotdatacollector;

import java.time.LocalDateTime;
import java.lang.annotation.Documented;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.Semaphore;
import org.bson.Document;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.CountDownLatch;
import com.mongodb.client.FindIterable;
import com.mongodb.BasicDBObject;
import java.util.Iterator;
import com.google.common.collect.Lists;
import java.util.List;

public class Coinplot {

	static String getCoinbaseDataAndReturnString(String _coinin ){
        MongoCollection<Document> collection;
        String connectString = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27020,dornhost.com:27018,dornhost.com:27019/coinmarketcap?replicaSet=replica&readPreference=primary";
        MongoClientURI connectionString = new MongoClientURI(connectString);
        MongoClient mongoClient = new MongoClient(connectionString);
        MongoDatabase db = mongoClient.getDatabase("coinbase");
        collection = db.getCollection( _coinin );
        BasicDBObject query = new BasicDBObject("_id", "-1");
        FindIterable<Document> lastdoc = collection.find();
        lastdoc.sort(new BasicDBObject("last_entry",-1));
        Iterator iterator = lastdoc.iterator();
        Document result = (Document) iterator.next();
        Document pricedata = (Document) result.get("data");
        String price = (String) pricedata.get("amount");
        mongoClient.close();
	    return price.toString();
	}

	static List<Document> returnArraySync( String _coinin ){

		MongoClient mongoClient = new MongoClient( new MongoClientURI( "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27020,dornhost.com:27018,dornhost.com:27019/coinmarketcap?replicaSet=replica&readPreference=primary" ));
		MongoDatabase db = mongoClient.getDatabase("coinbase");
		FindIterable<Document> lastdoc = db.getCollection( _coinin ).find().sort( new BasicDBObject("last_entry",-1) ).limit(1000);
		Iterator myIterator = lastdoc.iterator();
		List<Document> myList = Lists.newArrayList(myIterator);
        mongoClient.close();
		return myList;
	}

	public static Boolean GetCoinBaseDataAndInsertIntoDataBaseCompressed(String pairin, String databasein) {
		CallbackFunctions functions = new CallbackFunctions();
		Future<HttpResponse<JsonNode>> future = Unirest.get("https://api.coinbase.com/v2/prices/"+pairin+"/buy")
				 .asJsonAsync(new Callback<JsonNode>() {

				    public void failed(UnirestException e) {
				        System.out.println("The request has failed");
				    }
				    public void completed(HttpResponse<JsonNode> response) {
						 JsonNode body = response.getBody();
						 //System.out.println(body.toString());
				         functions.InsertDocIntoCollectionCoinbase( functions.ResponseBodytoDocumentForCoinBase(body) , databasein , pairin );
				       //  System.out.println("Hello");
				    }
				    public void cancelled() {
				        System.out.println("The request has been cancelled");
				    }
				});
		//System.out.print("Got here");
		return future.isDone();
	}
	
	public static Boolean GetCoinMarketCapDataAndInsertIntoDataBaseCompressed(String coinin, String databasein) {
	    CallbackFunctions functions = new CallbackFunctions();
	    String url = "https://api.coinmarketcap.com/v1/ticker/"+coinin;
	    Future<HttpResponse<JsonNode>> future = Unirest.get(url)
				 .asJsonAsync(new Callback<JsonNode>() {
				    public void failed(UnirestException e) {
				        System.out.println("The request has failed");
				    }
				    public void completed(HttpResponse<JsonNode> response) {
						
						JsonNode body = response.getBody();
						 
				        functions.InsertDocIntoCollectionCoinMarketCap(functions.ResponseBodytoDocumentForCoinMarketCap(body),databasein,coinin);
					
					}
				    public void cancelled() {
				        System.out.println("The request has been cancelled");
				    }
				});
		//System.out.print("Got here");
		return future.isDone();
	}
}


class CallbackFunctions {

	public Document ResponseBodytoDocumentForCoinMarketCap(JsonNode responsebodyinput) {
		String output = responsebodyinput.toString().replace("[","");
		output = output.replace("]","");
		return Document.parse(output);
	}


	public Document ResponseBodytoDocumentForCoinBase(JsonNode responsebodyinput) {
		String wordToFind = "warnings";
		Pattern word = Pattern.compile(wordToFind);
		Matcher match = word.matcher(responsebodyinput.toString());
		String substring = null;
		while (match.find()) {
			substring = responsebodyinput.toString().substring(0,match.start()-2)+"}";
		}
		return Document.parse(substring);
	}

	public void InsertDocIntoCollectionCoinMarketCap(Document documentin,String databasein,String collectionin) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		Logger rootLogger = loggerContext.getLogger("org.mongodb.driver");
		rootLogger.setLevel(Level.OFF);
		Date date = new Date();
		MongoCollection<Document> collection;
		String connectString = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27020,dornhost.com:27018,dornhost.com:27019/coinmarketcap?replicaSet=replica&readPreference=primary";
		MongoClientURI connectionString = new MongoClientURI(connectString);
	    MongoClient mongoClient = new MongoClient(connectionString);
	    MongoDatabase db = mongoClient.getDatabase(databasein);
	    collection = db.getCollection(collectionin);
	    long numberofdocs = collection.count();
	    documentin.append("date", date.toString());
	    documentin.append("date2",LocalDateTime.now().toString());
	    documentin.append("month",LocalDateTime.now().getMonthValue());
	    documentin.append("day",LocalDateTime.now().getDayOfMonth());
	    documentin.append("year",LocalDateTime.now().getYear());
	    documentin.append("last_entry", numberofdocs);
	    collection.insertOne(documentin);
	    mongoClient.close();

	}

	public void InsertDocIntoCollectionCoinbase(Document documentin,String databasein,String collectionin) {
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		Logger rootLogger = loggerContext.getLogger("org.mongodb.driver");
		rootLogger.setLevel(Level.OFF);
		Date date = new Date();
		MongoCollection<Document> collection;
		String connectString = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27020,dornhost.com:27018,dornhost.com:27019/coinbase?replicaSet=replica&readPreference=primary";
		MongoClientURI connectionString = new MongoClientURI(connectString);
	    MongoClient mongoClient = new MongoClient(connectionString);
	    MongoDatabase db = mongoClient.getDatabase(databasein);
	    collection = db.getCollection(collectionin);
	    long numberofdocs = collection.count();
	    documentin.append("date", date.toString());
	    documentin.append("date2",LocalDateTime.now().toString());
	    documentin.append("month",LocalDateTime.now().getMonthValue());
	    documentin.append("day",LocalDateTime.now().getDayOfMonth());
	    documentin.append("year",LocalDateTime.now().getYear());
	    documentin.append("last_entry", numberofdocs);
	    collection.insertOne(documentin);
	    mongoClient.close();
	}
}