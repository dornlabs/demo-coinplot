package coinplotdatacollector;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


class CollectorThreadCoinBase implements Runnable {
	String pairin;
	String databasein;
	Semaphore sem;
	CollectorThreadCoinBase(String pairin, String databasein, Semaphore _sem){
		this.pairin = pairin;
		this.databasein = databasein;
		this.sem = _sem;
	}	
	public void run(){
		try{
			this.sem.acquire();
		} catch( Exception e){};
		
		boolean isfinished = Coinplot.GetCoinBaseDataAndInsertIntoDataBaseCompressed(this.pairin, this.databasein);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp+   ConsoleColors.GREEN_BOLD_BRIGHT +    "::coinbasecollection:" +ConsoleColors.RESET+ConsoleColors.WHITE_BACKGROUND+ConsoleColors.PURPLE_BOLD_BRIGHT+" update " +ConsoleColors.RESET
		+ ConsoleColors.RESET + ConsoleColors.CYAN_BOLD_BRIGHT + this.pairin+" has been written to "+this.databasein+ ConsoleColors.RESET );
		try{
			this.sem.release();
		} catch( Exception e){};
		
	}
}

class CollectorThreadCmc implements Runnable {
	String pairin;
	String databasein;
	Semaphore sem;
	CollectorThreadCmc(String pairin, String databasein, Semaphore _sem){
		this.pairin = pairin;
		this.databasein = databasein;
		this.sem = _sem;
	}
	public void run(){
		try{
			this.sem.acquire();
		} catch( Exception e){};

		boolean isfinished = Coinplot.GetCoinMarketCapDataAndInsertIntoDataBaseCompressed(this.pairin, this.databasein);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp+   ConsoleColors.GREEN_BOLD_BRIGHT +    "::coinmarketcapcollection:" +ConsoleColors.RESET+ConsoleColors.WHITE_BACKGROUND+ConsoleColors.PURPLE_BOLD_BRIGHT+" update " +ConsoleColors.RESET
		+ ConsoleColors.RESET + ConsoleColors.CYAN_BOLD_BRIGHT + this.pairin+" has been written to "+this.databasein+ ConsoleColors.RESET );
		try{
			this.sem.release();
		} catch( Exception e){};
		

	}
}


public class Main {

	public static void main(String[] args) throws IOException {
		

		Semaphore sem = new Semaphore(1);
		ExecutorService executors = Executors.newSingleThreadExecutor();

		while(true) {

			executors.submit(new CollectorThreadCoinBase("btc-usd", "coinbase", sem));
			executors.submit(new CollectorThreadCoinBase("eth-usd", "coinbase", sem));
			executors.submit(new CollectorThreadCoinBase("ltc-usd", "coinbase", sem));
			executors.submit(new CollectorThreadCmc("bitcoin", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("ethereum", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("litecoin", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("neo", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("dogecoin", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("iota", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("ontology", "coinmarketcap", sem));
			executors.submit(new CollectorThreadCmc("zcash", "coinmarketcap", sem));
			
			try {
				Thread.sleep(300000);

			} catch (Exception e) {
			}
		}

	}
}
