cd application \
    && curl -s "https://get.sdkman.io" | bash \
    && source "$HOME/.sdkman/bin/sdkman-init.sh" \
    && sdk install gradle 4.9 \
    && gradle build \
    && gradle run \
    && bash
