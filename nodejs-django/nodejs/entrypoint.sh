 #!/usr/bin/env bash
echo "Servers Starting" \
	&& cd coinplot-frontend \
	&& npm install \
	&& npm install -g typescript \
	&& npm install @types/node --save \
	&& echo "building typescript" \
	&& tsc -p tsconfig.json \
	&& cd class \
	&& tsc -p tsconfig.json \
	&& cd .. \
	&& pm2 start index_main.js \
	&& cd .. \
	&& cd coinplot-trading-system \
	&& npm install \
	&& pm2 start index_socket.js \
	&& pm2 logs



