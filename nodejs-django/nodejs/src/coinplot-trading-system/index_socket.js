let args = {

    message : 'server up on port: '+ 9010,
    port : 9010

}

function main( _callback, _args ){

    let express = require('express')
    let app = express()
    let http = require('http').createServer( app ).listen( _args.port , _callback( _args.message ) )
    let io = require('socket.io').listen( http );
    let utilities = require('./utilties.js')
    let routes = require('./routes.js')( app )
   

    io.on( 'connection' ,  ( _socket )  => {

        let args = [ app , io , _socket ]

        setImmediate(  ( _args )=>{

            let app = _args[0]
            let io = _args[1]
            let socket = _args[2]
            let status = require( "./trade.js" )( io , socket );

        } , args )

    })

}


let callback = function( _input ) {

    console.log( _input )

}

main( callback , args )
