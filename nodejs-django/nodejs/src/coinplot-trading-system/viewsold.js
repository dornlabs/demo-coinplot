exports.responselogiccallback = (req, res) => {
    console.log("got a hit")
 
    let MongoClient = require('mongodb').MongoClient;
    
    const url = 'mongodb://aws-cp-aint-1-m.dornlabs.com:27017,aws-cp-aint-2-w.dornlabs.com:27018,aws-cp-aint-3-w.dornlabs.com:27019/?replicaSet=replica';
    const dbName = 'coinbase_averageprices'
    const dbName2 = 'coinbase'
    const collectionltc_string =  "ltc_average_price"
    const collectioneth_string = "eth_average_price"
    const collectionbtc_string = "btc_average_price"
  

    const collection_string_currentprice_eth = "eth-usd"
    const collection_string_currentprice_btc = "btc-usd"
    const collection_string_currentprice_ltc = "ltc-usd"



    let return2d = (num_in) => {
          
        return parseFloat(Math.round(num_in * 100) / 100).toFixed(2);
      
    }
    
    var spawn = require('child_process').spawn;
    let runpython = (spawin_in) => {
        const pypath = "python3"
        const pymain = "src/main.py"
        var output = spawin_in(pypath,[pymain])
        return output
    } 
    var exeoutput = runpython(spawn)
    
    //let dbpromiseforaverage = utilties.accessDbAsyncAndExecCallBacks(url,dbName).methods.getDbPromise()
    //let dbpromiseforcurrentprice = utilties.accessDbAsyncAndExecCallBacks(url,dbName2).methods.getDbPromise()
    
    var promiseWrapper = (collection_string_in, dbName_in)=>{

        var promise= new Promise( (resolve, reject)=>{
    
        MongoClient.connect(url, (err, db)=>{
            if (err) throw err;
                
            let dbo = db.db(dbName_in)
    
            dbo.collection(collection_string_in).find().sort({_id:-1}).limit(1).toArray( (err, result)=>{
    
                let poped = result.pop()
                resolve(poped)
                
                }) 
                
        })
        
    })
        return promise
    }
        

    Promise.all([   promiseWrapper(collectioneth_string,dbName),
                    promiseWrapper(collectionltc_string,dbName),
                    promiseWrapper(collectionbtc_string,dbName),
                    promiseWrapper(collection_string_currentprice_eth,dbName2),
                    promiseWrapper(collection_string_currentprice_ltc,dbName2),
                    promiseWrapper(collection_string_currentprice_btc,dbName2)
                    
                    
                    ]).then(   (values)=>{
        
                            var context = {
        
                            "eth": return2d(JSON.stringify(values[0]["average_price"])),
                            "ltc": return2d(JSON.stringify(values[1]["average_price"])),
                            "btc": return2d(JSON.stringify(values[2]["average_price"])),
                            "eth_high": return2d(JSON.stringify(values[0]["eth_daily_high"])),
                            "ltc_high": return2d(JSON.stringify(values[1]["ltc_daily_high"])),
                            "btc_high": return2d(JSON.stringify(values[2]["btc_daily_high"])),
                            "eth_low": return2d(JSON.stringify(values[0]["eth_daily_low"])),
                            "ltc_low": return2d(JSON.stringify(values[1]["ltc_daily_low"])),
                            "btc_low": return2d(JSON.stringify(values[2]["btc_daily_low"])),
                            "eth_cur": return2d((JSON.stringify(values[3]["data"]["amount"])).replace(/['"]+/g, '')),
                            "ltc_cur": return2d((JSON.stringify(values[4]["data"]["amount"])).replace(/['"]+/g, '')),
                            "btc_cur": return2d((JSON.stringify(values[5]["data"]["amount"])).replace(/['"]+/g, '')),
                
                            }
    
    
    
                            res.render("templates/index",context)
        
        
    })
      
}

