    
//UDP helper functions
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint16Array(buf));
  }

function str2ab(str) {
    var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i=0, strLen=str.length; i<strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
}
let dgram = require('dgram');
let udp = dgram.createSocket('udp4');
    udp.on( 'message' , ( _msg , _rinfo ) => { 
        
        let rec_message_package = JSON.parse( ab2str( _msg ) )
        

        console.log(`server got: ${ab2str( _msg )} from ${ _rinfo.address }:${ _rinfo.port }`);
   
    })
    
    udp.on('listening', () => {
        const address = udp.address();
        console.log(`server listening ${address.address}:${address.port}`);
      })
    
    udp.bind(1234);