const mcache = require('memory-cache')

let cache = (duration) => {
    return (req, res, next) => {
      let key = '__express__' + req.originalUrl || req.url
      let cachedBody = mcache.get(key)
      if (cachedBody) {
        res.send(cachedBody)
        return
      } else {
        res.sendResponse = res.send
        res.send = (body) => {
          mcache.put(key, body, duration * 1000);
          res.sendResponse(body)
        }
        next()
      }
    }
  }



let views = require('./views.js')

module.exports = function( _app ){

  
                
            _app.set('view engine','ejs')
    
            _app.get('/', cache(300), views.responselogiccallbackDefault ) 
            
    
}





