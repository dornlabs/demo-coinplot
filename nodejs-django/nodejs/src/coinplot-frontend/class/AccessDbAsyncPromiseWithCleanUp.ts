var mongodb = require('mongodb')
var MongoClient = mongodb.MongoClient;
interface AccessDbAsyncPromiseWithCleanUpValidator {

    urlofmongo:string,

    databasename:string,

    timeout:number,

    collection?:any

}
export class AccessDbAsyncPromiseWithCleanUp implements AccessDbAsyncPromiseWithCleanUpValidator{
    
    urlofmongo:string;

    databasename:string;

    timeout:number = 1500;

    collection:any;

    mongooptions:any;

    constructor( _urlin, _dbnamein ){
        
        this.urlofmongo = _urlin

        this.databasename = _dbnamein

        this.mongooptions = <any>{};

        this.mongooptions =  { server: {    

            auto_reconnect: true,    

            keepAlive: 1,    

            connectTimeoutMS: 60000,    

            socketTimeoutMS: 60000,

            useNewUrlParser: true,

        },

        }
    
    }
    getDbPromise(){

        return new Promise ( ( resolve , reject )=>{

            let output = {

                "message" : "Mongodb attempted connection to: " + this.urlofmongo + " dbname: "+ this.databasename + " in class/AccessDbAsyncPromise.getDbPromise",

            };

            try{

                MongoClient.connect( this.urlofmongo , this.mongooptions.server , ( err, connDB )=>{    

                    if(!err){

                        try{

                            var db = connDB.db( this.databasename );

                        }catch( _err ){

                            console.log(_err);
                        
                        }
                        if ( db != null && typeof(db) != undefined ){
                            
                            resolve( db )
                            
                            setTimeout( ()=>{

                                connDB.close();

                            } , this.timeout );

                        } else if ( db == null || typeof(db) == undefined) {

                            reject( db )

                            setTimeout( ()=>{

                                connDB.close();

                            } , this.timeout );

                        console.log( 'database promise rejected db returned null, error: '+ err )

                        }

                }

            });

        } catch ( _err ) {
                    
                    console.log( ( "Exception in class/AccessDbAsyncPromise.getDbPromise" ) );          
                    
                    console.log(   "Mongodb error connecting to: ", this.urlofmongo , " dbname: ", this.databasename );
                    
                    console.log(   "Exception caught and passed: " + _err );                        

                }
        })
    }
}