interface argsValidator {
    port:number,
    message:string
}
let args:argsValidator = {    
    port : 9000,
    message : 'server up on port: 9000',
}
function main( _callback : any , _args: argsValidator ){
    let express = require('express')
    let app = express()
    require('http').createServer( app ).listen( _args.port , _callback( _args.message ) )
    require('./utilties')
    require('./routes')( app )
}
let callback = function( _input:string ):void{
    console.log( _input )
}
main( callback , args )