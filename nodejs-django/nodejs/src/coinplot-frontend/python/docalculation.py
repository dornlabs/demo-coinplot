import datetime
import motor.motor_asyncio
import asyncio
import pandas as pd
import time
import pymongo
from modules.utilities import getAverageCount, getTodaysTime, getCollectionsSync

from termcolor import colored
from pymongo import MongoClient
from socketIO_client_nexus import SocketIO, BaseNamespace


#socketIO = SocketIO('http2.coinplot.info', 9010)
#socketIO.wait(seconds=1)
#socketIO.emit('chat message',"average price caculation connected")


#AVERAGE DATABASE ONLY FOR THE SYNC CLENT TO RETURN THE COUNT. USED FOR THE INDEX SORT FEATURE

daytuple = getTodaysTime()
counttuple = getAverageCount()
collectiontuple = getCollectionsSync()
print(daytuple)
month_int = int(daytuple[0])
today_int = int(daytuple[1])
year_int  = int(daytuple[2])

for document in collectiontuple[0].find().sort([('_id',-1)]).limit(288):
    print(document)
for document in collectiontuple[1].find().sort([('_id',-1)]).limit(288):
    print(document)
#for document in collectiontuple[2].find().sort([('_id',-1)]).limit(288):
#    print(document)
#MAIN DATABASE ASYNC





#NEW DATABASE WHERE THE AVERAGE PRICE WILL BE STORED
url_cpaverage  = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018/coinbase_averageprices"
client_sync_cbaverafe = MongoClient(url_cpaverage)

async def do_find_ltc():
    cursor = collectiontuple[0].find({"$and": [{"day":today_int},{"month":month_int},{"year":year_int}]}).sort([('_id', -1)])
   

async def do_find_btc():
    cursor = collectiontuple[1].find({"$and": [{"day":today_int},{"month":month_int},{"year":year_int}]}).sort([('_id', -1)])
        
async def do_find_eth():
    cursor = collectiontuple[2].find({"$and": [{"day":today_int},{"month":month_int},{"year":year_int}]}).sort([('_id', -1)])

loop = asyncio.get_event_loop()
        
tasks = [
        asyncio.ensure_future(do_find_eth()),
        asyncio.ensure_future(do_find_btc()),
        asyncio.ensure_future(do_find_ltc()),
    ]

loop.run_until_complete(asyncio.wait(tasks))
#socketIO.emit('chat message',"average price calculation finished")

#printer()

#socketIO.emit('chat message',"average price caculation disconnected")