import datetime
import motor.motor_asyncio
import asyncio
import pandas as pd
import time
import pymongo
from termcolor import colored
from pymongo import MongoClient
from socketIO_client_nexus import SocketIO, BaseNamespace

def getAverageCount():
    url_cb_avg ="mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018,dornhost.com:27019,dornhost.com:27020/coinbase_averageprices?replicaSet=replica&readPreference=primary"
    client_sync = MongoClient(url_cb_avg)
    db_sync = client_sync['coinbase_averageprices']
    ltc_average_price_collection_sync = db_sync['ltc_average_price']
    btc_average_price_collection_sync = db_sync['btc_average_price']
    eth_average_price_collection_sync = db_sync['eth_average_price']
    ltc_count = ltc_average_price_collection_sync.count()    
    btc_count = btc_average_price_collection_sync.count()
    eth_count = eth_average_price_collection_sync.count()
    return (ltc_count, btc_count, eth_count)

def getTodaysTime():
    today_int = int((((str(pd.datetime.today()).split("-")[2]).split("T"))[0]).split(" ")[0])
    month_int =int(str(pd.datetime.today()).split("-")[1])
    year_int = int(str(pd.datetime.today()).split("-")[0])
    return ( month_int, today_int, year_int)

def average_of_list(list_input):
    size = len(list_input)
    sum = 0
    for item in list_input:
        sum = sum+float(item)
    return sum/float(size)

def getCollectionsSync():
    url_cb = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018/coinbase"
    client_sync_coinbase = MongoClient(url_cb)
    db_sync = client_sync_coinbase["coinbase"]
    ltc_collection = db_sync["ltc-usd"]
    btc_collection = db_sync["btc-usd"]
    eth_collection = db_sync["eth-usd"]
    return (ltc_collection,btc_collection,eth_collection)