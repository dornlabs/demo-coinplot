let utilties = module.exports

utilties.accessDbAsyncAndExecCallBacks = function(prop1,prop2) {
    
    this.prop1 = prop1
    this.prop2 = prop2

    this.methods = {

        getDbPromise : ()=>{
            let url_in = this.prop1;
            let dbName_in   = this.prop2;
            console.log("Mongodb Connected on: ",url_in," dbname: ",dbName_in)
                return new Promise ( (resolve, reject)=>{
                    MongoClient.connect(url_in, (err, connDB)=>{
                        if (err) throw err;
                        let db = connDB.db(dbName_in);
                        resolve(db)
                    }) 
                })
            //    console.log("you must first set the connection string property using the setter")
            },
        execAsync : function (callback, callback2, collection, doc){
        //    (async ()=>{
        //        let db = await this.returnDbPromise;
                callback
               // example passed function
                //  db_object.collection(collection_string_in).insert(doc_in, (err, result) => {
               //     if(err){console.log("an error puting the provider status in the database")
               //     }else{console.log("provider status updated sucessfully "+JSON.stringify(result))}
                }
        }
    }

    exports.accessDbAsyncAndExecCallBacks2 = function( _prop1 , _prop2) {
        this.methods = {
            getDbPromise : function() {
                var url_in = _prop1;
                var dbName_in  = _prop2;
                console.log("Mongodb attempted connection to: ",url_in," dbname: ",dbName_in)
                    return new Promise ( (resolve, reject)=>{
                        try{
                            MongoClient.connect(url_in, (err, connDB)=>{
                                var db = connDB.db(dbName_in);
                                if (db!=null){
                                    resolve(db)
                                    console.log("Mongodb sucessfully connected to: ",url_in," dbname: ",dbName_in)
                                    console.log( 'database promise resolved db returned' )
    
                                }else if(db==null){
                                    reject(db)
                                    console.log( 'database promise rejected db returned null' )
                                }
                            })
                        }catch( _err ) {
                            console.log("Mongodb error connecting to: ",url_in," dbname: ",dbName_in)
                            console.log("Exception caught and passed: " + _err)                       
                        }
                    })
                }
        }
    }