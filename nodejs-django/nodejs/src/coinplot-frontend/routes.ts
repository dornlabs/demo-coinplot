var views = require('./views.js');
const mcache = require('memory-cache')
const request = require('request');

let cache = (duration) => {
    return (req, res, next) => {
      let key = '__express__' + req.originalUrl || req.url
      let cachedBody = mcache.get(key)
      if (cachedBody) {
        res.send(cachedBody)
        return
      } else {
        res.sendResponse = res.send
        res.send = (body) => {
          mcache.put(key, body, duration * 1000);
          res.sendResponse(body)
        }
        next()
    }
  }
}

module.exports = function(app){
  
            app.set('view engine','ejs')
            //app.get('/', cache(60) , (req, res)=>{
            //  res.write("<script>setTimeout(()=>{window.location.replace('https://coinplot.info/views/')},3000);</script>");
            //  res.write("down for upgrade");
            //  res.end();  
            //}); 
    
            app.get('/', views.responselogiccallback2 ) 
            
            app.get('/trade', cache(60) , views.responselogiccallback_trade )
            
            app.get('/createtrade', cache(60) , views.responselogiccallback_createtrade )
            app.get('/vue', cache(60) , views.responselogiccallback_vue )
            app.get('/createtrade/:input', function(req, res) {
              res.write(req.params.input);
              res.end();
            });
            

            app.get('/etharry', function(req, _res) {
              request('http://dornhost.com:7777/etharry', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });
            app.get('/btcarry', function(req, _res) {
              request('http://dornhost.com:7777/btcarry', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });
            app.get('/ltcarry', function(req, _res) {
              request('http://dornhost.com:7777/ltcarry', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });

            app.get('/eth', function(req, _res) {
              request('http://dornhost.com:7777/eth', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });
            app.get('/btc', function(req, _res) {
              request('http://dornhost.com:7777/btc', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });
            app.get('/ltc', function(req, _res) {
              request('http://dornhost.com:7777/ltc', { json: true }, (err, res, body) => {
                if (err) { return console.log(err); }
                  _res.write(JSON.stringify(body));
                  _res.end();
                });
            });
    
}





