'use strict';

const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://aws-cp-aint-1-m.dornlabs.com:27017,aws-cp-aint-2-w.dornlabs.com:27018,aws-cp-aint-3-w.dornlabs.com:27019/?replicaSet=replica';
const sleep = require('sleep')
const ejs = require('ejs')

const dbName = 'coinbase_averageprices';
const collection =  "ltc_average_price"
const collectioneth = "eth_average_price"
const collectionbtc = "btc_average_price"

var spawn = require('child_process').spawn;
var observe = require('observe')

const pypath = "/home/ubuntu/python3env/bin/python3.5"
const pymain = "/var/www/html/kodcloud/data/User/admin/home/document/composetest/nodejs/coinplotnodejsviews/main.py"
const fs = require('fs')
var object = {a:[]}
var observer = observe(object)

let runpython = (spawin_in,pypath_in, pymain_in)=>{
    output = spawin_in(pypath_in,[pymain_in])
    return output
} 




function main(){
    
    
    app.listen(8000, ()=> console.log("server up"))
    
    
    
}




app.get('/', (req, res)=>{
    
    
    observer.on('change', (change)=>{
        
       if (observer.subject.a.length==3){
           
           var array = observer.get('a').subject
            
           var context = {name:new Array(array[0]["average_price"], array[1]["average_price"], array[2]["average_price"])}
            
           console.log(array[0]["average_price"], array[1]["average_price"], array[2]["average_price"]) 
          
           observer.set('a',[])
           
           app.set('view engine','ejs')
           res.render("templates/chart",context)
           
       } 
    })
        
        
        
        
    MongoClient.connect(url, (err, db)=>{
        if (err) throw err;
            
        let dbo = db.db(dbName)

        dbo.collection(collection).find().toArray( (err, result)=>{
            let popedltc = result.pop()
            observer.get('a').append(popedltc)
            }) 
            
        
        dbo.collection(collectionbtc).find().toArray( (err, result)=>{
            let popedbtc = result.pop()
            
            observer.get('a').append(popedbtc)
            })    
            
            
        dbo.collection(collectionbtc).find().toArray( (err, result)=>{
            let popedbtc = result.pop()
            
            observer.get('a').append(popedbtc)
            }) 
           
        })
        



})



app.get('/chart/', (req, res) => {
    app.set('view engine','ejs')
    var context = {name:"Welcome to Charts 2"}
    
    
    res.render("templates/chart",context)
    
    
})




app.get('/ltc/', (req, res)=>{

    
    exeoutput = runpython(spawn,pypath,pymain)
  
    
    exeoutput.stdout.on('data', (data)=>{
        
        
        MongoClient.connect(url, (err, db)=>{
            if (err) throw err;
            
            let dbo = db.db(dbName)
            dbo.collection(collection).find().toArray( (err, result)=>{
                let poped = result.pop()
                console.log(poped)
                res.send(poped)
  
            })            
            
            
            
        })
        
        
        console.log(data.toString());
    
        
    })
  // Close connection
  
})

app.get('/eth/', (req, res)=>{

    
    exeoutput = runpython(spawn,pypath,pymain)
  
    
    exeoutput.stdout.on('data', (data)=>{
        
        
        MongoClient.connect(url, (err, db)=>{
            if (err) throw err;
            
            let dbo = db.db(dbName)
            dbo.collection(collectioneth).find().toArray( (err, result)=>{
                let poped = result.pop()
                console.log(poped)
                res.send(poped)
  
            })            
            
            
            
        })
        
        
        console.log(data.toString());
    
        
    })
  // Close connection
  
})

app.get('/btc/', (req, res)=>{

    
    exeoutput = runpython(spawn,pypath,pymain)
  
    
    exeoutput.stdout.on('data', (data)=>{
        
        
        MongoClient.connect(url, (err, db)=>{
            if (err) throw err;
            
            let dbo = db.db(dbName)
            dbo.collection(collectionbtc).find().toArray( (err, result)=>{
                let poped = result.pop()
                console.log(poped)
                res.send(poped)
  
            })            
            
            
            
        })
        
        
        console.log(data.toString());
    
        
    })
  // Close connection
  
})



main()