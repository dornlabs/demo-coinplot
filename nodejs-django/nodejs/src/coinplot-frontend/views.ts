import { AccessDbAsyncPromiseWithCleanUp as AccessDbAsyncPromise } from './class/AccessDbAsyncPromiseWithCleanUp';
exports.responseLogicMaterial = (req,res) => {
    let context = {}
    res.render("templates/material",context)
}
exports.responselogiccallback_trade = ( req, res ) =>{
    let context = {};
    res.render("templates/index",context);    
}
exports.responselogiccallback_createtrade = ( req, res ) =>{
    let context = {};
    res.render("templates/createtrade",context);    
}
exports.responselogiccallback_vue = ( req, res ) =>{
    let context = {};
    res.render("templates/vue",context);    
}
exports.responselogiccallback2 = (req, res) => {     
    const dbName = 'coinbase_averageprices'
    const dbName2 = 'coinbase'

    const pypath = "python3"
    const pymain = "/workspace/coinplot-frontend/python/docalculation.py"
    
    let spawnSync = require( 'child_process' ).spawnSync;
    let result = spawnSync( pypath , [pymain] );

    if (result.status !== 0) {
        process.stderr.write(result.stderr);
        process.exit(result.status);
      } else {
        process.stdout.write(result.stdout);
        process.stderr.write(result.stderr);
    }

    let return2d = (num_in:string):string => {
        var float_return:string = parseFloat(num_in).toFixed(2);
        return float_return;
    }
    
    let dbpromisedbname1 =<any>{};
    const urlcoinbase_avg = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018,dornhost.com:27019,dornhost.com:27020/coinbase_averageprices?replicaSet=replica&readPreference=primary"; 
    dbpromisedbname1 = new AccessDbAsyncPromise( urlcoinbase_avg , 'coinbase_averageprices' ).getDbPromise();
    
    let dbpromisedbname2 =<any>{};
    const urlcoinbase = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018,dornhost.com:27019,dornhost.com:27020/coinbase?replicaSet=replica&readPreference=primary"; 
    dbpromisedbname2 = new AccessDbAsyncPromise( urlcoinbase, 'coinbase' ).getDbPromise();

    let dbpromisedbname3 =<any>{};
    const urlcoinmarketcap = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018,dornhost.com:27019,dornhost.com:27020/coinmarketcap?replicaSet=replica&readPreference=primary"; 
    dbpromisedbname3 = new AccessDbAsyncPromise( urlcoinmarketcap , 'coinmarketcap' ).getDbPromise();
    

    let dbpromisedbname4 =<any>{};
    const urlcoinmarketcapaverage = "mongodb://root:UTRDXIACTVKPCQDXOHVMHZXHCVXRHXZDHA9BUDAWIAKYSU9ULSBOQPPIBYFYPVBHU9BYPGTWIPCGHLVFY@dornhost.com:27018,dornhost.com:27019,dornhost.com:27020/coinmarketcap?replicaSet=replica&readPreference=primary"; 
    dbpromisedbname3 = new AccessDbAsyncPromise( urlcoinmarketcapaverage , 'coinmarketcap_averageprices' ).getDbPromise();


    let main = async function(){
        try{
            var db1 = await dbpromisedbname1;
            var db2 = await dbpromisedbname2;

            var ethvalue = await db1.collection( "eth_average_price" ).find().sort(  {"count":-1} ).limit(1).toArray();
            var ltcvalue = await db1.collection( "ltc_average_price" ).find().sort( {"count":-1} ).limit(1).toArray();
            var btcvalue = await db1.collection( "btc_average_price" ).find().sort( {"count":-1} ).limit(1).toArray();
            var ethprice = await db2.collection( "eth-usd" ).find().sort( { _id:-1 } ).limit(1).toArray();
            var ltcprice = await db2.collection( "ltc-usd" ).find().sort( { _id:-1 } ).limit(1).toArray();
            var btcprice = await db2.collection( "btc-usd" ).find().sort( { _id:-1 } ).limit(1).toArray();       
        }catch( err ){
            console.log(err);
        }
        var values = [ ethvalue.pop() , ltcvalue.pop() , btcvalue.pop() , ethprice.pop() , ltcprice.pop() , btcprice.pop() ];
        var context = {
            "eth": return2d(JSON.stringify(values[0]["average_price"])),
            "ltc": return2d(JSON.stringify(values[1]["average_price"])),
            "btc": return2d(JSON.stringify(values[2]["average_price"])),
            "eth_high": return2d(JSON.stringify(values[0]["eth_daily_high"])),
            "ltc_high": return2d(JSON.stringify(values[1]["ltc_daily_high"])),
            "btc_high": return2d(JSON.stringify(values[2]["btc_daily_high"])),
            "eth_low": return2d(JSON.stringify(values[0]["eth_daily_low"])),
            "ltc_low": return2d(JSON.stringify(values[1]["ltc_daily_low"])),
            "btc_low": return2d(JSON.stringify(values[2]["btc_daily_low"])),
            "eth_cur": return2d((JSON.stringify(values[3]["data"]["amount"])).replace(/['"]+/g, '')),
            "ltc_cur": return2d((JSON.stringify(values[4]["data"]["amount"])).replace(/['"]+/g, '')),
            "btc_cur": return2d((JSON.stringify(values[5]["data"]["amount"])).replace(/['"]+/g, '')),
        }
        res.render("templates/index", context)
    }
    main();
}
